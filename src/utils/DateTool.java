package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTool {

	public static String DATE_PATTERN = "dd/MM/yyyy";
	public static String DATE_TIME_PATTERN = "dd/MM/yyyy hh:mm:ss";

	public static String formatDate(Calendar calendar) {
		return format(calendar, DATE_PATTERN);
	}

	public static Calendar parseDate(String string) throws ParseException {
		return parse(string, DATE_PATTERN);
	}

	public static String formatDateTime(Calendar calendar) {
		return format(calendar, DATE_TIME_PATTERN);
	}

	public static Calendar parseDateTime(String string) throws ParseException {
		return parse(string, DATE_TIME_PATTERN);
	}

	private static String format(Calendar calendar, String pattern) {
		return new SimpleDateFormat(pattern).format(calendar.getTime());
	}

	private static Calendar parse(String string, String pattern) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Calendar calendar = Calendar.getInstance();
		Date date = simpleDateFormat.parse(string);
		calendar.setTime(date);
		return calendar;
	}
}
