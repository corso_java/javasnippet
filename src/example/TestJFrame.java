package example;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TestJFrame {
	public static void main(String[] args) {
		JFrame frame = new JFrame("Il mio titolo");
		JPanel jPanel = new JPanel();
		JButton button = new JButton("Fai click");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Sono stato premuto");

			}
		});
		jPanel.add(button);
		jPanel.setBackground(Color.RED);
		frame.add(jPanel);
		frame.setVisible(true);
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
