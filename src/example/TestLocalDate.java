package example;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class TestLocalDate {

	public static void main(String[] args) {
		/*
		 * Data corrente (al presente articolo 26/01/2019) LocalDate today =
		 * LocalDate.now(); Data specficiando anno, mese, giorno LocalDate yesterday =
		 * LocalDate.of(2019, 01, 25); Una data specificando una stringa in formato ISO
		 * LocalDate tomorrow = LocalDate.parse("2019-01-27");
		 */
		LocalDate localDate = LocalDate.now();
		System.out.println(localDate);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.ITALY);
		System.out.println(localDate.format(formatter));
		DateTimeFormatter formatter2 = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(Locale.ITALY);
		System.out.println(localDate.format(formatter2));
		LocalTime localTime = LocalTime.now();
		System.out.println(localTime);
		LocalDateTime localDateTime = LocalDateTime.now();
		System.out.println(localDateTime.plusDays(10));
		System.out.println(formatter2.format(localDateTime.plusDays(10)));
		localDateTime = localDateTime.plusDays(10);
		System.out.println(localDate.isBefore(localDateTime.toLocalDate()));
	}
}
