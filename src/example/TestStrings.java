package example;

public class TestStrings {

	public static void main(String[] args) {
		String string = new String("Ciao");
		System.out.println(string.equalsIgnoreCase("ciao"));
		System.out.println(string.equals("ciao"));
		System.out.println(string.length());
		System.out.println(string.substring(2));
		System.out.println(string.charAt(3));
		System.out.println(string.indexOf("i"));
		System.out.println(string.startsWith("ciao"));
		System.out.println(string.endsWith("iao"));
		System.out.println(string.toLowerCase());
		System.out.println(string.toUpperCase());

		String myLongString = "Ciao mondo come va?";
		System.out.println(myLongString.replace("mondo", "terra"));
		System.out.println(myLongString);
		System.out.println(myLongString.replaceFirst("a", "|"));

		String output = "Hai speso %d €";
		System.out.println(String.format(output, 93));

		String multipleOutput = "Ciao %1s e %2s e %d";
		System.out.println(String.format(multipleOutput, "Luca", "Giovanni", 12));

		String empty = "";
		System.out.println(empty.isEmpty());

		System.out.println(isUpperCase(myLongString));
		System.out.println(firstCharUpper("giovanni"));
	}

	private static boolean isUpperCase(String myString) {
		return myString.equals(myString.toUpperCase());
	}
	
	private static String firstCharUpper(String myString) {
		return myString.substring(0, 1).toUpperCase()+myString.substring(1).toLowerCase();
	}

}
