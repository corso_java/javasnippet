package example;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import utils.DateTool;

public class TestTokenizer {

	private static String SEPARATOR_TOKEN = ";";

	public static void main(String[] args) throws ParseException {
		String row = "Mario;Rossi;12/07/1980;";
		// String row = String.join(";", "Mario","Rossi";"12/07/1980");
		StringTokenizer stringTokenizer = new StringTokenizer(row, SEPARATOR_TOKEN);
		String name = stringTokenizer.nextToken();
		String surname = stringTokenizer.nextToken();
		Calendar born = DateTool.parseDate(stringTokenizer.nextToken());

		String[] parts = row.split(SEPARATOR_TOKEN);
		for (String string : parts) {
			System.out.println(string);
		}

		List<String> results = Pattern.compile(SEPARATOR_TOKEN).splitAsStream(row).collect(Collectors.toList());
		results.forEach(s -> System.out.println(s));
	}
}
