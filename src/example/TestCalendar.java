package example;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class TestCalendar {
	public static void main(String[] args) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.MONTH, 2);
		calendar.set(Calendar.YEAR, 2019);
		//calendar.set(year, month, date);
		//calendar.set(year, month, date, hourOfDay, minute, second);
		System.out.println(calendar.get(Calendar.MONTH));
		Date date = calendar.getTime();
		System.out.println(date);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		System.out.println(simpleDateFormat.format(date));
		DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.SHORT);
		System.out.println(dateFormat1.format(date));
		DateFormat dateFormat2 = DateFormat.getDateInstance(DateFormat.FULL);
		System.out.println(dateFormat2.format(date));
		String myTextDate = "01-02-2019 12:10:50";
		System.out.println(simpleDateFormat.parse(myTextDate));
		//Calcolo della differenza di giorni tra due date
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.set(2019, Calendar.FEBRUARY, 10);
		cal2.set(2019, Calendar.FEBRUARY, 22);
		Date firstDate = cal1.getTime();
		Date secondDate = cal2.getTime();
		Long days = ChronoUnit.DAYS.between(firstDate.toInstant(), secondDate.toInstant());
		System.out.println("Days between the two dates: "+days);
		// Aumento di 15 giorni una data
		Calendar calendar15 = Calendar.getInstance();
		calendar15.add(Calendar.DATE, 15);
		System.out.println(calendar15.getTime());
	}
}
